This chapter explains how to use the configuration file \texttt{conf.json} to
shape part of the \cd{} functionalities after the compilation.

First of all, the \texttt{conf.json} file has to be placed in the \texttt{root folder} of the \cd{} library or the \cd{} server in order to be read correctly. In case the file is not provided the \cd{} will use default
parameters. Instead, since version 9.7, a malformed conf file will shutdown the
\cd{}.

In order to check whether the \texttt{conf.json} was succesfully loaded the
signal \texttt{ConfigChecksum} reports the CRC of the configuration file. In case
the configuration file is not found, the \texttt{ConfigChecksum} reports ``-1''.

\section{Communication parameters}
The key  called ``\texttt{ServerCommunication}'' allows the
user to set the IP address from which \cd{} receive the message and the port used by the \cd{} Server when it starts.
\\
\begin{table}[!h]
\begin{center}
\begin{tabularx}{0.9\textwidth}{lcX}
\hline 
Name & Default & Description \\ 
\hline
\hline
\texttt{"FromIP"} &  ``127.0.0.1'' & Only messages scenarios sent from this interface (which match the bound name) are routed to the \cd{} application. \\
\texttt{"Port"} &  30000 & The binding port of the server. \\
\hline
\end{tabularx}
\caption{Server communication parameters.}
\end{center}
\end{table}


\clearpage
\section{Internal Parameters}
Under the key ``\texttt{Internal}'' the user can set the parameters of the
internal functioning of \cd{} divided in groups based on the use:

\subsection{Internal Configuration Parameters}
\begin{table}[!h]
	\centering
	\begin{tabularx}{0.9\textwidth}{lccX}
		\hline Name & Default & Dim & Description \\ 
		\hline
		\hline
		\texttt{"steerRangeAmplitudeFactor"} & 1.0 &  [rad/(m s)]  & scale range of lateral jerks \\
		\texttt{"longRangeAmplitudeFactor"} & 1.0 & [m/s$^3$] & Scale longitudinal jerks range \\
		\texttt{"nTol"} & 0.02 & [m]  & Lateral tolerance for arcs definition \\
		\texttt{"maxspan"} & 50 & [m] &Maximum distance for arcs definition \\
		\texttt{"LevisonGain"} & 1.0 &  [m/s] & scale target velocity in curves\\
		\hline
	\end{tabularx}
	\caption{Internal Configuration Parameters.}
\end{table}


\subsection{General Parameters}
\begin{table}[!h]
	\centering
	\begin{tabularx}{0.9\textwidth}{lccX}
		\hline Name & Default & Dim & Description \\ 
		\hline
		\hline
			\texttt{"useLRM"} 		&  0 & [-]	& Enable the use of the LRM module\\
		\texttt{"ActionSelectionMechanism"} & 0 &[-] &  Action selection mechanism:
		\begin{tabular}{@{}l@{}}
			(0) WTA\\
			(1) MSPRT
		\end{tabular}\\
		\texttt{"exportTrajectoryType"} 	& 0&	[-]	& 
		\begin{tabular}{@{}l@{}}
			(0) export path by \cd{}\\ 
			(1) input curvature profile \\
			(2) lane lines overlay
		\end{tabular}\\
	\texttt{"followPathEnable"} 		&  0&[-]	& 
	\begin{tabular}{@{}l@{}}
	(0) lateral states update based on sensors reading \\  
	(1) internal prediction for path tracker
\end{tabular}\\
	\texttt{"cameraCurvature"} 		&  0 &	[-]& 
\begin{tabular}{@{}l@{}}
	(0) Instantaneous curvature from the map \\  
	(1) Inst. curvature from the camera use signal \texttt{LaneCrvt}
\end{tabular}\\
	\texttt{"compensateHeading"}		& 0	&[-]& compensate heading due to camera in CarMaker \\
	\texttt{"estimateSideSlip"}			& 0	&[-] & compensate heading due to side slip angle in CarMaker \\
	\texttt{"ObstacleCourse"} & 0.0	& [rad]& force obstacles course to a fixed value, 0.0 means course from the sensors' detection \\
	\texttt{"ForceOneLane"} & 0	&[-]&  force \cd{} to stay on the current lane\\
	\texttt{"UseLowSpeedPrimitive"} 	&  0& [-]	&Enable the use a free flow primitive for low speed  \\
	\texttt{"steeringGain"} 	&  50.0 & [m]	& Steer gain use for the curvature estimation  (c = delta\_wheel/L/steer\_gain means that steeringGain = L*steer\_gain\\
	\hline
	\end{tabularx}
\caption{General parameters.}
\end{table}

\clearpage
\subsection{Optimal Control Parameters}
\begin{table}[!h]
	\centering
	\begin{tabularx}{0.9\textwidth}{lccX}
		\hline Name & Default&Dim & Description \\ 
		\hline
		\hline
	\texttt{"AccWeight"} 				&  0.4 & [m$^2$/s$^6$]	& acceleration cost for the longitudinal optimal  control problem\\
	\texttt{"timeWeight"} 				& 0.25 & [1/s]	& cost of overall manouevre time execution\\
				\hline
\end{tabularx}
\caption{Optimal control parameters.}
\end{table}

\subsection{Velocity Thresholds}
\begin{table}[!h]
\centering
\begin{tabularx}{0.9\textwidth}{lccX}
\hline Name & Default& Dim  & Description \\ 
\hline
\hline
	\texttt{"LowSpeedPrimitiveThreshold"} 	& 5.0 &[m/s] & Velocity threshold for low speed freeflow primitive \\
	\texttt{"minVLgtFild"} 	&  1.0& [m/s]	& It is the minimum velocity used  \\
	\texttt{"lowSpeedSteerThreshold"} 	& 1.0 &[m/s]	&  The steer is disabled below this velocity threshold \\
	\texttt{"lowSpeedCurvatureThreshold"} 	& 5.0&[m/s]& The computation of the curvature change using this threshold if the longitudinal velocity is below this threshold the curvature is computed using $\frac{\delta}{\text{SteerGain}}$ otherwise is computed using $\frac{\Omega}{V_g}$\\
				\hline
\end{tabularx}
\caption{Velocity Thresholds.}
\end{table}

\subsection{Obstacles Parameters}
\begin{table}[!h]
\centering
\begin{tabularx}{0.9\textwidth}{lccX}
\hline Name & Default&Dim & Description \\ 
\hline
\hline
	\texttt{"redZoneLen"} 	& 0.5 & [m]	& length of obstacle inhibition longitudinal box \\ 
	\texttt{"yellowZoneTime"} 	& 1.0 &	[s] & duration of obstacle warning longitudinal box \\
	\texttt{"snTolObstacle"} 	&  70.0	& [m] & tolerance for detecting orthogonal obstacless \\
				\hline
\end{tabularx}
\caption{Obstacles Parameters.}
\end{table}

\clearpage
\subsection{Lookahead Distance Parameters}
The lookAhead is determined using the following equation:
\begin{equation}
\texttt{lookAhead}=\text{Max}\left[\texttt{lookAheadMin}, \texttt{linearLookAhead}\cdot\texttt{VLgtFild}, \frac{\texttt{VLgtFild}^2}{2\cdot \texttt{requireDecelerationPolicy}}\right]
\end{equation}
\begin{table}[!h]
	\centering
	\begin{tabularx}{0.9\textwidth}{lccX}
		\hline Name & Default&Dim & Description \\ 
		\hline
		\hline
		\texttt{"lookAheadMin"} 	& 50 & [m]	& minimum lookahead distance for electronicHorizon and obstacle \\ 
		\texttt{"linearLookAhead"} 	& 10 &	[s] & Linear velocity look ahead \\
		\texttt{"requireDecelerationPolicy"} & 1.0 & [m/s$^2$]& Deceleration policy \\
		\hline
	\end{tabularx}
	\caption{Obstacles Parameters.}
\end{table}

\subsection{Lateral Salience Parameters}
\begin{table}[!h]
\centering
\begin{tabularx}{0.9\textwidth}{lccX}
\hline Name & Default&  Dim & Description \\ 
\hline
\hline
	\texttt{"timeLateralSalience"}		& 5.0 & [s] &  max time considered for planning lateral primitive, (shorter time is more robust against map uncertainties) \\
	\texttt{"gainLateralSalienceMax"}		& 1.4 & [-] &  factor used to scale the lateral salience of the right lane \\
	\texttt{"gainLateralSalienceMin"}		& 0.7 &[-] &  factor used to scale the lateral salience of the left lane \\
		\texttt{"lateralMarginAllowedPercentage"}  & 0.5 &[\%] & percentage of available lateral margin allowed $\frac{(\text{lane width} - \text{car width})}{2}$. if set to 1 all margin allowed\\
					\hline
\end{tabularx}
\caption{Lateral Salience Parameters.}
\end{table}

\subsection{Codriver stimuli bias}
\begin{table}[!h]
	\centering
	\begin{tabularx}{0.9\textwidth}{lccX}
		\hline Name & Default & Dim & Description \\ 
		\hline
		\hline
		\texttt{"ExternalLongBiasWeight"} 	&  0& [-]	&  Longitudinal bias for codriver stimuli \\
		\texttt{"ExternalLatBiasWeight"} 	& 0	& [-]  & Lateral bias for codriver stimuli \\
		\hline
	\end{tabularx}
	\caption{Codriver stimuli bias.}
\end{table}

\clearpage
\subsection{Neural Network for Pedestrian Safety Parameters}
\begin{table}[!h]
\centering
\begin{tabularx}{0.9\textwidth}{lccX}
\hline Name & Default & Dim & Description \\ 
\hline
\hline
	\texttt{"useNNforPedestrianSafety"} &  0 &[-]	& Enable neural network for safe speed pedestrian approaching  \\
		\texttt{"NNforPedestrianSafetyLoop"} & 0 &[-] & Enable the Safety loop network\\
	\texttt{"NNforPedestrianSafetyVelOffset"}  & 0.0 & [m/s]& bias longitudinal velocity of the vehicle for low speed tests\\
	\texttt{"NNforPedestrianSafetyVelStep"}  & 0.05	& [m/s] &  velocity reduction step at each time step the network is called\\
	\texttt{"NNforPedestrianSafetyPedMargin"}  & 0.5&[m]	&  Pedestrian Safety margin\\
		\texttt{"NNforPedestrianSafetyMaxPedDist"}  & 20.0& [m]	&  Tolerance to detect pedestrian in orthogonal direction\\
	\hline
\end{tabularx}
\caption{Neural network for pedestrian safety parameters.}
\end{table}

\section{Action Selection}
Under the key ``\texttt{ActionSelection}'' the user can set the parameters of
the MSPRT action selection algorithm in case it was previously selected.

\begin{table}[!h]
\centering
\begin{tabularx}{0.9\textwidth}{lccX}
\hline Name & Default & Dim & Description \\ \hline
\hline
\texttt{"wThrsh"} & 0.0001 &[-] & thereshold for accepting action\\
\texttt{"forgFactor"}  & 5 & [-]& forgetting factor when the maximum number of
windows has been reached\\
\hline
\end{tabularx}
\caption{Default parameters used by MSPRT.}
\end{table}

\clearpage
\section{Low level models}
Under the key ``\texttt{LowLevelParameters}'' the user can choose which low
level control to use and set the parameters of each options.

\subsection{Combined low level control}
To date, there are no combined controls.
\begin{table}[!h]
	\centering
	\begin{tabularx}{\textwidth}{lcX}
		\hline Name & Code & Description \\ 
		\hline
	\hline
	\texttt{"Comb"} & 0	& independent longitudinal and lateral low level control selected\\
					            & 10  &(only MXNET build) test neural network\\
	\hline
	\end{tabularx}
	\caption{Possible choices for combined low level controllers.}
\end{table}

\clearpage
\subsection{Longitudinal low level control}
List of the longitudinal low level control that can be enabled.
The output of the longitudinal controller is written into the ``\texttt{LongitudinalControlAction}'' a 30 double vector signal. 
\begin{table}[!h]
	\centering
	\begin{tabularx}{\textwidth}{ccX}
		\hline 
		Name & Code & Description\\ 
		\hline
	\hline
	\texttt{"Long"} &  -1  & PI controller on the target \\
					&  0	& no low-level longitudinal control\\
					& 1	& sample long. acceleration from polynomial primitive\\
					& 2	& integrate long. acceleration from pol. primitive\\
					%& : & "20" & {\color{red} neural network for DFKI vehicle
					%(output is pedal position) \textbf{REQUIRES MXNET} }\\
					& 10  & inverse model CRF MIL\\
				 	& 20 & neural network for DFKI MIA vehicle, output is pedals position:  LongitudinalControlAction[0]=throttle, LongitudinalControlAction[1]=brake \\
					 & 21	& neural network for DFKI MIA vehicle v2.0, output is pedals position: LongitudinalControlAction[0]=throttle,	LongitudinalControlAction[1]=brake \\
					& 22	& (only MXNET build) neural network for DFKI MIA\\
	\hline
	\end{tabularx}
	\caption{Possible choices for longitudinal dynamics low level controllers.}
\end{table}

\subsubsection{Parameters of the low level control}
Under the key ``\texttt{Parameters}'' the user can set the parameters of the previously chosen low level model. The parameters of those controller at listed in the table~\ref{tab:longparams}.
\begin{table}[!h]
	\centering
	\begin{tabularx}{\textwidth}{llcXc}
		\hline
		\texttt{Code} & \texttt{Name} & \texttt{Default} & \texttt{Description} &  \texttt{Dim}\\
		\hline 
		\hline
		-1 		& 	\texttt{"velTarget"} 	& 0	 & target velocity for PI control (enable if velTarget $>$ 0.5) & [m/s]	\\	
		-1 		& 	\texttt{"kp"} 			& 0.2 & proportional gain on velocity error & [1/s]\\
		-1 		& 	\texttt{"kp"} 			& 0.1	 & integral gain on velocity error 	& [1/s$^2$]\\
		1,2							&	\texttt{"longGain"}		&  1    & gain of the longitudinal integrator & [-]\\
		1,2,10,20,21		&	\texttt{"maxAcc"} 		&2   & maximum longitudinal acceleration & [m/s$^2$]\\
		1,2,10,20,21    	&	\texttt{"minAcc"} 		& 6   & minimum longitudinal acceleration & [m/s$^2$]\\
		2							&	\texttt{"velReset"}	 	& 0.01 & velocity to reset longitudinal integrator	 		& [m/s]\\
		2							&	\texttt{"enableReset"}	&true & trigger reset of longitudinal integrator			    & [-]\\
		\hline
	\end{tabularx}
	\caption{Possible parameters for each longitudinal control type.}	\label{tab:longparams}
\end{table}


\clearpage
\subsection{Lateral low level control}
The output of the lateral controller is written into the ``\texttt{LateralControlAction}'' 30 double vector signal.

\begin{table}[!h]
	\centering
	\begin{tabularx}{\textwidth}{ccX}
		\hline 
		Name &  Code & Description \\ 
		\hline
		\hline
	\texttt{"Lat"} 	&  -1  & simple trajectory tracker \\
					&  0	& no low-level lateral control\\
					
					&  1	& sample curvature from polynomial\\
					&  2 	& sample steering wheel angle from polynomial\\
					&  3	& sample yaw-rate from polynomial\\
					
					& 5 	& integrate curvature using integrator\\
					&  6	& integrate steering wheel angle using integrator\\
					&  7 	& integrate yaw-rate using integrator\\
					&  8	& integrate steering wheel angle single track model using integrator\\
					
					& 10	& neural network MIL Jeep Renegade actuator inverse dynamics\\
					&  11	& neural network MIL Jeep Renegade vehicle + actuator inverse dynamics\\
					&  12	& bias neural network MIL Jeep Renegade vehicle + actuator inverse dynamics\\
					&  13	& bias robust neural network MIL Jeep Renegade vehicle + actuator inverse dynamics\\
					&  131	& bias robust neural network MIL Jeep Renegade vehicle with online adaptation \\
					&  14  & bias neural network with integral based adaption MIL Renegade\\
					
					& 15	& bias neural network VEH Jeep Renegade vehicle + actuator inverse dynamics\\
					& 151	& old version of 15 \\
					&  16	& bias robust neural network VEH Jeep Renegade vehicle + actuator inverse dynamics\\
					& 161	& old version of 16 \\
					&  17	& bias neural network with integral based adaption VEH Renegade\\
					&  20	& neural network for DFKI MIA vehicle steering wheel angle\\
					&  201	& neural network for DFKI MIA vehicle steering wheel angle + integrator\\
					&  21	& network DFKI MIA steering wheel angle Klettwitz data curvature from steering\\
					&  22	& network DFKI MIA steering wheel angle Klettwitz data curvature from yaw-rate\\
					& 23	& network DFKI MIA steering wheel angle Klettwitz data curvature integrator (adaptation)\\		
					
					& 31 & (Only Acado build) MPC Acado \cite{Houska2011ACADO} Jeep Renegade MIL single track + actuator, Mathematica fit\\
					& 33 & (Only Acado build) MPC Acado \cite{Houska2011ACADO} Jeep Renegade MIL single track model, Mathematica fit \\
					& 34 & (Only Acado build) MPC Acado \cite{Houska2011ACADO} Jeep Renegade MIL single track model, MatLab fit \\
					& 35 & (Only Acado build) MPC Acado \cite{Houska2011ACADO} OpenDS model\\
					
					& 40	& (only MXNET build) neural network for CarMaker\\
					
					&  51	& bias neural network MIL Single Track Jeep Renegade\\
					& 52	& bias neural network MIL Single Track Jeep Renegade (curvature from $a_y$)\\
					& 53	& bias neural network MIL Single Track Jeep Renegade round 2\\
					
					& 60	& neural network for 500X CarMaker\\
	\hline
	\end{tabularx}
	\caption{Possible choices for lateral dynamics low level controllers.}
\end{table}

\clearpage
\subsubsection{Parameters of the low level control}
Using the same section ``\texttt{Parameters}'' the user can set the parameters of the previously chosen low level lateral control.
\begin{table}[!h]
	\centering
	\begin{tabularx}{\textwidth}{llcXc}
		\hline
		\texttt{Code} & \texttt{Name} & \texttt{Default} & \texttt{Description} &  \texttt{Dim}\\
		\hline 
		\hline
			-1,2,6,8,10	&	\texttt{"steerGain"} 	 & 50.0   & gain for the linear and single track lateral model 	& [m]\\
			-1,2,6,8	&	\texttt{"maxSteer"} 	 & 1.5  & maximum steering wheel angle & [rad]\\
				1,5		&	\texttt{"maxCurv"} 		& 0.5  & maximum curvature									& [1/m]\\
				5,23		&	\texttt{"curvGain"}  & 1.0  & gain for the curvature integrators 					& [-]\\
				8,10	& 	\texttt{"kUSgrad"}	 	& 0.0 & undesteering gradient\footnote{note that the undesteering gradient is here normalized on the vehicle wheelbase w.r.t. the tradtional formulation of the $K_{\text{us}}$}	& [s$^2$/m$^2$]\\
	\hline
	\end{tabularx}
	\caption{Possible parameters for each lateral control type.}\label{tab:lat_params}
\end{table}

\section{Filter}
Under the key ``\texttt{Filter}'' the user can set the parameters of the window
of the moving average filters. The signals in \cref{tab:filters_params} can be
filtered.

\begin{table}[!ht]
\centering
\begin{tabularx}{\textwidth}{lc}
\hline Name & Default dimension of the time window \\ \hline
\hline
\texttt{"ObjAcc"} & 0 \\
\texttt{"ObjCourseRate"} & 0 \\
\texttt{"LatOffs"} & 0 \\
\texttt{"LaneWidth"} & 0\\
\texttt{"VLgtFild"} & 0 \\
\texttt{"YawRateFild"} & 0 \\
\hline
\end{tabularx}
\caption{List of available signals for filtering and default window size.}\label{tab:filters_params}
\end{table}
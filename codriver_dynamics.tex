% ---- curvilinear coordinates ----
% \input{trajectory}

\section{Vehicle control}
This chapter explains how to interpret the signals in the manoeuvre message to compute 
proper vehicle inputs in terms of steering wheel angle, acceleration reference etc. depending 
on the specific application. 
Despite the \cd{} having built in several low level models for vehicle control (described in \ref{ch:configuration}) 
in case the user wants to design a custom low level model.
 
\subsection{Longitudinal dynamics}
Typically the longitudinal dynamics can be addressed by providing an acceleration reference to the vehicle.
For that, the user can compute the reference acceleration by reconstructing the polynomial relationship of the optimal jerk.
\begin{itemize}
\item $J_0$: vehicle desired jerk at $t=t_0$;
\item $S_0$: vehicle desired snap at $t=t_0$;
\item $Cr_0$: vehicle desired crackle at $t=t_0$;
\end{itemize}
From which you can compute the requested jerk $J_{req}$ as:
\begin{equation}
J_{req} = J_0 + S_0 \cdot \tau + \frac{1}{2} Cr_0 \cdot \tau ^2,
\end{equation}\label{eq:opt_jerk}
where $\tau = 0.05$ is the time step.

From \eqref{eq:opt_jerk} one can compute the reference acceleration in two ways:
\begin{enumerate}
    \item integrate using current acceleration as initial value:
        \begin{equation}
            a_{req} = a_0 + \tau J_{req},
        \end{equation}
    \item integrate using internal state integrator:
        \begin{align}
            a_{req} &= a_{int} + \tau J_{req}\\
            a_{int} &= a_{req}
        \end{align}
\end{enumerate}

Integrator reset to avoid wind-up is helpful.
In particular when the velocity \texttt{VLgtFild} falls below 
the threshold a certain threshold it is a good idea to reset the internal state.
In addition the integrator is saturated such that the output is bounded 
by the maximum acceleration $a_{max} = 1.0$ [m/s$^2$] and 
minimum acceleration  $a_{min} = -3.5$ [m/s$^2$] .

\subsection{Lateral dynamics}
The \cd{} outputs the following parameters:
\begin{itemize}
	\item \texttt{Sn0}: is the lateral position, $n(t)$ at $t=t_0$ [m]; 
	\item \texttt{Alpha0}: is the relative heading to the lane $b(t)$ at $t=t_0$ [rad];
	\item \texttt{Delta0}: is the absolute curvature of the ego trajectory $c(t)-k0$ at $t=t_0$ [1/m]. Where $c(t)$ is the relative curvature and $k_0$ is the curvature of the lane;
	\item \texttt{Jdelta0}: desired time derivative of trajectory curvature $r(t)$ [rad/(m s)], relative to the lane multiplied by $V^2$ at $t=t_0$ [rad m/s$^3$];
	\item \texttt{Sdelta0}: desired second time derivative of trajectory curvature relative to the lane moltiplied $V^2$ at $t=t_0$ [rad m/s$^4$];
	\item \texttt{Crdelta0}: desired third time derivative of trajectory curvature relative to the lane moltiplied $V^2$ at $t=t_0$ [rad m/s$^5$];
\end{itemize}
This coefficients are the results of an optimal control problem based on the following dynamic model:
$$
\begin{cases}
	n'(t)-V b(t)\\
	b'(t)-V c(t)\\
	\sum _{i=1}^z k_i \delta_{\text{Dirac}} \left(t-T_i\right)+c'(t)-r(t)
\end{cases}
$$
where $V$ is the velocity of the vehicle, $k_i$ represents the instantaneous variation of curvature at time $T_i$. $r$ represents the control action.\\

Then the request lateral jerk $r_{req} = j_{req}$ is computed:
\begin{equation}
	j_{req} = \text{jerk}_0 + \text{Snap}_0 \cdot \tau + \frac{1}{2} \text{Crackle}_0 \cdot \tau ^2,
\end{equation}
To obtain the value of jerk, snap and crackle of the optimal primitive at the correct time instant we need to divided by $V^2$ the coefficients get in the maneuver message.
Notice due to the formulation of the lateral dynamics of the \cd{} a division times $V^2$ is necessary  to compute the curvature in time domain.
\begin{equation}
j_{req} = \frac{1}{V^2}\left(\texttt{Jdelta0} +  \texttt{Sdelta0} \cdot \tau + \frac{1}{2}  \texttt{Crdelta0} \cdot \tau ^2\right),
\end{equation}
where $\tau = 0.05$ is the time step.
Integrating $j_{req}$ in the time we obtain the relative curvature. 

Typically vehicles are controlled through a requested steering wheel angle $\delta$. $\delta$ can be 
computed out of a lateral jerk (curvature rate) via integration of the jerk to obtain a curvature 
and further division by the a gain $K_{steer}$ representing the product of wheelbase times steering ratio. Similarly to the longitudinal case the integrator can have its own internal state or be memory-less.\\

Hence
\begin{equation}
    \delta = \left(\int{j_{req}} + k_0 \right) K_{steer},
\end{equation}
where typical values for $K_{steer}$ are around 50.
In general the $K_{steer}$ is not constant but for low lateral accelerations 
(i.e. the regions where the single track model approximation holds) 
you can assume $K_{steer}$ to be constant. The $K_{steer}$ is \texttt{steerGain} see table~\ref{tab:lat_params}.\\

To identify your $K_{steer}$ you should be able to plot the quantity:
\begin{equation}
\frac{\delta_{sw} \Psi}{\texttt{VLgtFild}},
\end{equation}
where $\Psi$ is the yaw rate and take first order approximation.


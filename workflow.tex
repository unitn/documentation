This chapter explains how to use and test the \cd{} agent.

\section{Running \cd{}}
The binaries of \cd{} are distributed to the project partners and potential
users via the Bitbucket public-bin repository
(\href{https://bitbucket.org/unitn/public-bin/src/master/}{link to
\textit{public-bin} repository}). 
Currently we support any OS (Windows, Mac OS, Linux) and architecture (x86/AMD64/aarch64). 

Starting from version v9.5 we defined different branches according to the OS/CPU
configuration. Hence before downloading the software make sure to be in the
proper branch to download the latest version.

\vspace{10mm}
\noindent
There are several options in which you can run the \cd{}:
\begin{enumerate}

\item \textbf{Executable}: the \cd{} code is compiled into an UDP server which communicates via the \textit{Scenario} and \textit{Manoeuvre} messages as
described in \cref{ch:interfaces}. The two server variants are:
\begin{itemize}
\item \textit{codriver\_server}: full implementation of the \cd{}  (self-driving code + communication code + logging code), please have a look at
Sec.~\ref{subsec:server} for clarification to how to use it. 
%
\item \textit{codriver\_server\_void}: in this case only logging+communication capabilities are provided for communication testing purposes.
\end{itemize}

\item\textbf{MatLab S-Function} \textsuperscript{\textregistered}: (Windows X64,  and MacOs) this option can be used in CarMaker4Simulink
\textsuperscript{\textregistered} environment for \textit{Model-in-the-Loop}
(MIL) tests. Together with the S-Function the buses (\texttt{codriver\_buses.mat}) needed for the creation of the scenario and
manoeuvre messages are provided. There are several S-Functions which can be
used:
\begin{description}
	\item[sfun\_codriver.mexw64]: full implementation of the \cd{} agent;
	\item[sfun\_UDPCoDriver\_remote.mexw64]: only communication side, a server instance of the \cd{} has to be run simultaneously to compute the manoeuvre
	message; 
	\item[codriver\_interfaces\_scenario\_serializer.mexw64]: needed for real-time hardware dSPACE\textsuperscript{\textregistered{}} MABX II
	serialization of scenario message;
	\item[codriver\_interfaces\_manoeuvre\_deserializer.mexw64]: needed for real-time hardware dSPACE\textsuperscript{\textregistered{}} MABX II
	deserialization of manoeuvre deserialize;
\end{description}

\item \textbf{Libraries}: full implementation of the \cd{} to be run locally (both dynamically and statically linked) without the overhead of the UDP
communication;
\begin{description}
	\item[lib\_static\_remote]: static library providing only UDP utility to
	communicate with a server instance;
	\item[lib\_static]: static library full implementation of the \cd{};
	\item[lib\_remote]: dynamic library providing only UDP utility;
	\item[lib]: dynamic library full implementation of the \cd{};  
\end{description}
\end{enumerate}

All the ways to execute \cd{} provide logging utility. Properly launched, each
time the \cd{}  is executed it saves data in two files:
\begin{itemize}
\item \textit{Scenario message}: is the log file that collects the input to the \cd{}; 
\item \textit{Manoeuvre message}: is the log file that collects the output of \cd{}.
\end{itemize}

\subsection{The root path}
\cd{} run in a \texttt{root path} that can be modified at run time. The root path is used as position for loading the conf file and for saving the log files. It always possible configure the root path using different ways depending which option are used for run \cd{}. Commonly the root path is the running folder.

\subsection{Using the server}\label{subsec:server}
Codriver server is the executable for \cd{}. 
The \cd{} server application can be used as follow:
\begin{enumerate}
	\item No parameters:\\
	\texttt{usage: codriver\_server}\\
	The server run with no log and the configuration file is loaded in the running folder.
	\item One or two parameters:\\
	\texttt{usage: codriver\_server log\_folder [log\_type]}\\
	\begin{itemize}
		\item 	\texttt{log folder}. When you add this parameters the \cd{} starts logging
		the scenario and the maneuver messages in the log folder indicated. If the
		folder does not exist it will be created for you. The conf file is loaded from the running folder.
		If \cd{} uses a configuration file this file is copied inside the log folder to take track
		of configuration parameters used in the correspondent simulation. Inside the
		log folder four files are created:
		\begin{enumerate}
			\item \texttt{YYYYMMDD\_HHMMSS\_scenario.bin}
			\item \texttt{YYYYMMDD\_HHMMSS\_manoeuvre.bin}
			\item \texttt{YYYYMMDD\_HHMMSS\_conf.json}
			\item \texttt{YYYYMMDD\_HHMMSS\_stat.json}
		\end{enumerate} 
		\item \texttt{log\_type}. If the parameter is missing the \texttt{log\_type = 1} (means standard log). The level of log is used to configure the different log possibility of internal parameters of the \cd{}. The level of log is an integer number but it is read as binary mask of 32 bits. Each bit is used to enable or disable a different log data.
	\\
		\begin{tabularx}{0.8\linewidth}{ccX} 
			\hline
			Binary & Integer & Description\\
			\hline
			\hline
			$0b0000000000000000$ & $0$ & Log disabled.\\
			$0b0000000000000001$ & $+1$ & Log both scenario and maneuver\\
			$0b0000000000000010$ & $+2$ & Log only scenario\\
			$0b0000000000000011$ & $+3$ & Log only maneuver\\
			$0b0000000000010000$ & $+16$ & Enable the logical reasoning module (LRM)
			log.\\
			$0b0000000000100000$ & $+32$ & Enable the action selection module (ASM)
			log.\\
			$0b0000000000100000$ & $+64$ & Enable the log of the pedestrian safety network.\\
			\hline
		\end{tabularx}
	\item More parameters:\\
	\texttt{usage: codriver\_server [option]}\\
	The codriver runs with some options:
	\begin{itemize}
		\item \texttt{-logdir [log\_folder]} configure the log folder and enable the log of type 1.
		\item \texttt{-logtype [log\_type]} change the type of the log the option if no \texttt{log\_folder} parameter is set. \cd{} creates a folder \texttt{log\/}.
		\item \texttt{-rootpath [root\_path]} modify the root path from the running folder to a custom folder. From this folder will be loaded the \texttt{conf} file and created the \texttt{log\_folder}.
	\end{itemize}
	\end{itemize}
\end{enumerate}
Some example of calls of codriver\_server in linux are:
\begin{itemize}
	\item \texttt{./codriver\_server} $\rightarrow$ The \cd{} server is called but it does not log and root path set on the running folder.
	\item \texttt{./codriver\_server log\/} $\rightarrow$ The \cd{} server
	logs scenario and manoeuvre in the ``log\/'' and if a conf file is present in the  in the running folder will be loaded and copied in the log folder.
	\item \texttt{./codriver\_server log 2} $\rightarrow$ The \cd{} server log only scenario inside the ``log'' folder.   
	\item \texttt{./codriver\_server log 33} $\rightarrow$ The \cd{} server log scenario, maneuver and action selection internals in the ``log" folder.  
	\item \texttt{./codriver\_server -logdir log -rootpath test/ } $\rightarrow$ The \cd{} server run in the folder \texttt{test} (loading the conf file from this folder) and create inside the ``test/log'' folder the log file. 
\end{itemize}

\clearpage
\subsection{Using the S-Function}
The S-Function provides the possibility of running the \cd{} both locally and in
a remote way in the Simulink environment using the UDP communication module
built-in in the S-Function (UDP communication module from Simulink not needed).

\noindent
The S-Function accepts 3 parameters as shown in \cref{fig:sfun_params} and
explained in \cref{table:sfun_params}.


\begin{figure}[!ht]
	\centering
	\includegraphics[width=0.7\textwidth]{fig/sfun_params}
	\caption{Parameters of S-Function.}\label{fig:sfun_params}
\end{figure}

\begin{table}[!h]
	\centering
	\begin{tabular}{cccc}
		\hline
		 	& Parameter 	& Datatype 			& example \\
		 	 \hline
		 	 \hline
		P1 	& IP address 	& [int,int,int,int]	&  $[127,0,0,1]$\\
		P2	& Port			& [int]				& 30000 \\
		P3 	& Log type	& [int] 			& 1     \\ 
		\hline
	\end{tabular}
	\caption{Parameters of S-Function description.}\label{table:sfun_params}
\end{table}
In order to run locally the \cd{}, the IP address has to be set to $[0,0,0,0]$.
The ``Log type'' parameter is defined in the same fashion as in
\cref{subsec:server}. In case the log is enabled, the S-Function creates a
``\textit{log}'' folder in the current working directory of MatLab. 
Using the S-Function is not possible change the running folder.
Similarly to the server execution the S-Function embedded \cd{} can be
configured using the \texttt{conf.json} file. Notice that the S-Function does
not read the communications parameters from the \texttt{conf.json} file, instead
communication parameters should be specified via Simulink GUI. In order to use
the configuration file the same has to be located in the working directory of
MatLab. The detailed description of the setting of the parameters is reported in
\cref{ch:configuration}.

\clearpage
\subsection{Using the library}
The \cd{} library is given for both static or dynamic linking for Windows, Mac,
Linux. There is 2 versions of the libraries. One versions works only in remote
mode ``codriver\_remote" and the second works both local and remote mode
``codriver\_public". In section~\ref{ch:publicbin} is explained how to download
all these libraries. Both libraries ``codriver\_remote" and ``codriver\_public"
export the same interface that is composed by seven main functions used for
working with \cd{}. 
The header file ``codriver\_public\_lib.h'' list the functions as follow:
\begin{lstlisting}[title={Functions of \cd{} library}, language=C++]
// Fuction for testing library loading
USE_DLL	void test_lib_codriver();

// Function to set path for log and conf file
// Set the root path of the conf and of the log file
USE_DLL void set_root_path(char *path);

// Set folder for the log
USE_DLL void set_log_folder(char *path);

/*  ------------------------------ CLIENT FUNCTIONS -----------------------------  */
// Client codriver init for matlab
USE_DLL	void client_codriver_init_num(unsigned int num_ip[4] = 0, int server_port = 0, int log_type = 0);

// Client codriver init
// log_on == 0 log disabled
// log_on != 0 log enabled
USE_DLL void client_codriver_init(const char *server_ip = 0, int server_port = 0, int log_type = 0);

// Client send
USE_DLL int client_send_to_server(volatile uint32_t server_run, uint32_t message_id, input_data_str* scenario_msg);

// Client receive
USE_DLL int client_receive_form_server(uint32_t *server_run, uint32_t *message_id, output_data_str* manoeuvre_msg, uint64_t start_time);

// Client for codriver compute: send and receive in one function
USE_DLL void client_codriver_compute(input_data_str* scenario_msg, output_data_str* manoeuvre_msg);

// Close codriver
USE_DLL void client_codriver_close();
/*  ------------------------------ CLIENT FUNCTIONS ----------------------------  */

/*  ------------------------------ SERVER FUNCTIONS ---------------------------  */
USE_DLL	void server_codriver_init_num(unsigned int num_ip[4], int server_port);

USE_DLL void server_codriver_init(const char *server_ip, int server_port);

USE_DLL int server_send_to_client(uint32_t server_run, uint32_t message_id, output_data_str* manoeuvre_msg);

USE_DLL int server_receive_from_client(uint32_t *server_run, uint32_t *message_id, input_data_str* scenario_msg);

USE_DLL char* server_receive_from();

USE_DLL void server_codriver_close();
/*  --------------------------- SERVER FUNCTIONS --------------------------------  */

/*  ---------------------------- UTILITY FUNCTIONS ------------------------------  */
// Return motor cortex
USE_DLL void get_motor_cortex(double motor_cortex[41][41]);

// Return flatten motor cortex
USE_DLL void get_flatten_motor_cortex(double motor_cortex[1681]);

// Return winning indexes
USE_DLL void get_winning_index(int idx[2]);

// Client Get time
USE_DLL uint64_t client_get_time_ms();
/*  ----------------------------- UTILITY FUNCTIONS ------------------------------  */
\end{lstlisting}  
\subsection{Configuration Functions}
The first three function are used for test the correct loading of the library (useful when is used the dynamic library).
The other two function modified the root path and the log folder. This function must be called before initialize \cd{}.

\subsection{Client Functions}
The functions inside the  comment `` CLIENT FUNCTIONS'' are used for communicate with \cd{}. 
The first two functions \texttt{client\_codriver\_init\_num} and
\texttt{client\_codriver\_init} are used for initializing the communication channel and the \cd{} parameters. The first function is created for be used in Matlab as the interface with c++ are more stable using a vector numbers instead a char*. The inputs of the function are:
\begin{itemize}
	\item \texttt{num\_ip}: a vector of 4 numbers between $0$ and $255$ (i.e.
	$[127,0,0,1]$). If is used a empty vector the library initializes a local
	\cd{}, so no UDP communication is opened. Of course if you are using the
	``codriver\_remote'' you can not use the local functionality. In this case
	the ip parameter is mandatory.
	\item \texttt{server\_ip}: IP string, it works as the \texttt{num\_ip}.
	\item \texttt{server\_port}: the \cd{} server port. The default port of
	\cd{} server is ``30000''. 
	\item \texttt{log\_type}: enable the log functionality and define the type of the log (works as the second parameter for \texttt{codriver\_server}). If \texttt{log\_type} is set to 1 a log folder is automatically generated. It is possible to configure the name of the log folder using the function \texttt{set\_log\_folder}. This parameter is used only if no IP is selected, meanly when the library works in local mode. The remote library does not used this parameters. 
\end{itemize}
The function \texttt{client\_codriver\_compute} is use to compute a maneuver
using a scenario by \cd{}. The parameters are:
\begin{itemize}
 	\item \texttt{scenario\_msg}: the scenario is a pointer of
 	\texttt{input\_data\_str}. The variables inside this structure are defined in the header file \texttt{``codriver\_interfaces\_data\_structs.h''}. You
 	need to fill this structure before calling this function. The function does not modify the fields.
 	\item \texttt{manoeuvre\_msg}: the maneuver is a pointer of \texttt{output\_data\_str}. This field has to be allocated and initialized by zeros before function is called. The function fills the structure with the maneuver parameters.
\end{itemize}
The function \texttt{client\_codriver\_close} is use to close the communication
channel or close the log files.

\subsection{Server Functions}
This functions are used inside the \cd{} server. This functions can be used if the user want to create a new \cd{} server with different characteristics. 


\subsection{Setting the parameters of conf.json}
The setup of the \cd{} using the configuration file is explained in
\cref{ch:configuration}.

\subsection{Converting the binary logged files}
\cd{} logs in binary format. In order to interpret the log In order to converted
*.bin to *.txt a \texttt{conveter} executable is provided.

\subsection{Re-generating log file with updated \cd{} releases}
The executable ``\texttt{codriver\_offline\_log\_generator}'' permits to
regenerate the maneuver message for a different configuration file setup or
agent version given a binary scenario file.

% \section{Interfaces file} The building procedure starts from the \texttt{.csv}
% file present in the ``codriver\_interfaces/data'' folder. The first important
% thing to notice is that there must be a \textbf{single}  \texttt{.csv} file in
% such folder. The  \texttt{.csv} is obtained from the
% ``D4C\_codriver\_interfaces\_vXX.XX.xlsx'' file. Currently there is no way to
% further automatize the export process from \texttt{.xlsx} to \texttt{.csv}
% file.

% The link to the most updated version of the interfaces file is reported in
% Chapter \autoref{ch:Interfaces}.

% Basically: \begin{itemize} \item Interfaces 11.13 $\Rightarrow$ ``Adaptive''
% project \cd{} interfaces; \item Interfaces 12.04 $\Rightarrow$ ``D4C'' project
% \cd{} interfaces; \end{itemize}

% \section{\cd{} Compilation from object file} External modules: \begin{itemize}
% \item \textit{Logical reasoning module:} takes as input a simplified scenario
% message and returns the bounding boxes;\\
% \item \textit{Action selection module:} takes as input the gain matrices
% $g_k$, the initial weights $w'_k$ and the semantic code $c_k$ associated to
% each bounding box and returns the gain $g$ matrix to compute multiply the
% motor cortex. \end{itemize}











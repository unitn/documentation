This chapter explains how to run \cd{} in OpenDS.

\section{Getting the OpenDS}
Currently we support a interfaces 11.13-based \cd{} on OpenDS.\\\vspace{5pt}
\noindent
Download the code from Syncplicity or BitBucket:\\\vspace{5pt}
{\color{blue} \href{https://my.syncplicity.com/share/bqtiufgnkfl9fbv/opends4.8_2018-02-26}{Syncplicity}}\\\vspace{5pt}
{\color{blue} \href{https://rafaelmath@bitbucket.org/rafaelmath/opends.git}{BitBucket}}\\
Alternatively the new interfaces based OpenDS (12.04) can be found at:
\\\vspace{5pt}
\noindent
{\color{blue} \href{https://bitbucket.org/unitn/public-bin-opends/src/a021822fa3ebcb955780cd5c647c9764cb6c1318/}{BitBucket 12.04}}\\

System requirements:
\begin{table}[!h]
\centering
\begin{tabular}{ll}
\hline
Operating system: & Windows, Linux, Mac OS\\
Memory &$ >$ 1 GB\\
CPU & $ >$ 1 GHz\\
Graphic Card & AMD/ATI Radeon 9500, NVIDIA GeForce 5 FX, Intel GMA 4500,\\ & or better supporting OpenGL 2.0 or better\\
Java Runtime Environment & JRE 8 or higher.\\
\hline
\end{tabular}
\end{table}
\noindent


\subsection{Getting started}
\begin{itemize}
\item unzip the OpenDS to any folder where you have write access;
\item Run \code{OpenDS.jar} (e.g ``\code{java -jar OpenDS.jar}'')
\item Select the following settings from the screen below:
\begin{figure}[!ht]
\centering
\includegraphics[width=8cm]{fig/openDSmain.png}
\end{figure}
\begin{itemize}
\item Fullscreen: NO
\item Vsync: NO
\item Screen Depth: 24 bpp
\item Anti-Aliasing: Disabled
\end{itemize}
\item Select Driving Task: assets/DrivingTasks/Projects/track1/track1.xml
\item Start
\end{itemize}

\subsection{Manually running the vehicle}
The car can be run using the following keys:\\
\begin{table}[!h]
\centering
\begin{tabular}{ll}
\hline
ARROW\_UP & accelerate\\
ARROW\_DOWN & accelerate backwards\\
ARROW\_LEFT & steer left\\
ARROW\_RIGHT & steer right\\
SPACE & brake\\
V & change camera view\\
F1 & detailed key mapping\\
ESC & shut down\\
\hline
\end{tabular}
\end{table}

\subsection{Running \cd{} in OpenDS}
The OpenDS version at the BitBucket repository has already embedded the \cd{} library (dynamic public library) which can work both locally or remotely.
Local execution (default) is the best option for performance and reliability, however remote execution can be set by changing the IP address of the init function.
In case of the remote (UDP) operating mode first run the \cd{} and then OpenDS.

\section{Creating the scenario}
This section explains how to create a user defined scenario.

\subsection{Assets folder}
The central folder for defining custom scenarios is the ``\textbf{Assets}'' folder.
\begin{itemize}
\item driving environments ``\textbf{Scenes}'',
\item scene objects ``\textbf{Models}'',
\item audio files ``\textbf{Sounds}'',
\item image files ``\textbf{Textures}'',
\item GUI definitions ``\textbf{Interface}'',
\item task description files ``\textbf{DrivingTasks}'',
\item templates for performance reports ``\textbf{JasperReports}''
\end{itemize}
The resources stored in the central assets folder can be accessed by multiple driving setups.\\

\subsection{Task Description Files} 
The ``\textbf{DrivingTasks}'' folder consists of the two subfolders ``\textbf{Projects}'' and ``\textbf{Schema}'':
\begin{itemize}
\item ``\textbf{Projects}'' contains some sample projects which typically consist of the following XML files:
\begin{description}
	\item[\code{openDRIVE.xodr}]
	\item[\code{scene.xml}] file contains the declaration of all objects that are relevant for a driving task. The declaration part is subdivided into the following sections: sounds (\cref{lst:OpenDriveSound}), images (\cref{lst:OpenDrivePicture}), 3D models (\cref{lst:OpenDriveModel}), geometries, reset points, gravity, and lights .
	\item[\code{scenario.xml}]
	\item[\code{interaction.xml}]
	\item[\code{settings.xml}]
	\item[\code{task.xml}]
\end{description}

\begin{figure}[!ht]
\centering
\begin{lstlisting}[language=XML, caption={The example code shows of a positional sound ``beep'' with given position, volume 50\%, and no changes to the pitch. When starting the playback, this file will only be played once (loop = false)}, label={lst:OpenDriveSound}]
<sound id="beep" key="Sounds/Effects/Beep.ogg"> 
	<positional value="true" > 
		<translation> 
			<vector>
				<entry>0</entry> 
				<entry>-5</entry> 
				<entry>3.5</entry> 
			</vector> 
		</translation> 
	</positional> 
	<directional value="false" /> 
	<loop>false</loop> 
	<volume>0.5</volume> 
	<pitch>1</pitch> 
</sound>
\end{lstlisting}
\end{figure}

\begin{figure}[!ht]
\centering
\begin{lstlisting}[language=XML,caption={The example code shows a transparent 100x100px picture ``image01'' which is visible and located 10px from top and 20\% from the left border of the simulation screen.}, label={lst:OpenDrivePicture}]
<picture id="image01" key="Textures/image01.png" level="1"> 
	<vPosition> 
		<fromTop unit="px" value="10" /> 
	</vPosition> 
	<hPosition> 
		<fromLeft unit="%" value="20" /> 
	</hPosition> 
	<width>100</width> 
	<height>100</height> 
	<useAlpha>true</useAlpha> 
	<visible>true</visible> 
</picture>
\end{lstlisting}
\end{figure}


\begin{figure}[!h]
\begin{lstlisting}[language=XML, caption={The code shows an example of a visible model named ``driverCar'' which has a mass of 1200 kg, a mesh-accurate collision shape, as well as a given rotation (95\degree around the up axis) and position (unit: meter)]}, label={lst:OpenDriveModel}]
<model id="driverCar" 
key="Models/Cars/drivingCars/Mercedes/Car.scene"> 
	<mass>1200</mass> 
	<visible>true</visible> 
	<collisionShape>meshShape</collisionShape> 
		<scale> 
			<vector jtype="java_lang_Float" size="3"> 
				<entry>1</entry> 
				<entry>1</entry> 
				<entry>1</entry> 
			</vector> 
		</scale> 
		<rotation quaternion="false"> 
			<vector jtype="java_lang_Float" size="3"> 
				<entry>0</entry> 
				<entry>95</entry> 
				<entry>0</entry> 
			</vector> 
		</rotation>
		<translation> 
			<vector jtype="java_lang_Float" size="3"> 
				<entry>-792.395</entry>
				<entry>0.969</entry> 
				<entry>-33.835</entry> 
			</vector> 
	</translation> 
</model>
\end{lstlisting}
\end{figure}

\item ``\textbf{Schema}'' contains the XML schema files for those XML files.
\end{itemize}

\begin{figure}[!ht]
\centering
\includegraphics[width=15cm]{fig/taskDescription.png}
\caption{Schematic view of Task Description files, courtesy of OpenDRIVE\textsuperscript{\textregistered{}}.}
\end{figure}

\subsection{OpenDRIVE Format}
The OpenDRIVE format (\texttt{.xodr}) represents a XML-like description for complex road network. All roads consists of a reference line which defines the basic geometry e.g. arcs, straight lines etc, Fig.\ref{fig:openDrive1}.

\begin{figure}[!ht]
\centering
\includegraphics[width=15cm]{fig/openDrive1.png}
\caption{OpenDRIVE road structure, courtesy of OpenDRIVE\textsuperscript{\textregistered{}}.}\label{fig:openDrive1}
\end{figure}

Along the reference line various properties of the road can be defined. e.g. lanes, elevation profile, traffic signs etc, Fig.\ref{fig:openDrive2}.

\begin{figure}[!ht]
\centering
\includegraphics[width=15cm]{fig/openDrive2.png}
\caption{OpenDRIVE lanes structure, courtesy of OpenDRIVE\textsuperscript{\textregistered{}}.}\label{fig:openDrive2}
\end{figure}


Roads can be linked to each other Fig.\ref{fig:openDrive3}
\begin{itemize}
\item directly when there is only one connection possible between two given roads;
\item via junctions when there is more than one connection to other roads.
\end{itemize}

\begin{figure}[!ht]
\centering
\includegraphics[width=15cm]{fig/openDrive3.png}
\caption{OpenDRIVE lanes structur, courtesy of OpenDRIVE\textsuperscript{\textregistered{}}e.}\label{fig:openDrive3}
\end{figure}

\subsection{Advanced Road Generation}


\begin{enumerate}
\item write  ``road description file'' \texttt{.xml};
\item run the ``sceneGenerator.exe'' to obtain \textbf{PointList} format
	\begin{itemize}
	\item $\Rightarrow$ ``pointlists.txt'' 
	\item $\Rightarrow$ ``terrain.obj''
	\end{itemize}
\item convert ``pointlists.txt'' to the \textbf{GeometryList} format using ``P2GConverter.exe'';
\item convert \textbf{GeometryList} to the \textbf{OpenDrive} (\texttt{.xodr}) format used by OpenDS using ``RoadGenerator.jar''.
\end{enumerate}


\subsection{Basic Road Generation}
\textbf{GeometryGenerator} can be used to rapidly create OpenDRIVE files.
\begin{itemize}
\item Edit \code{roadDescription.xml}
\item Run \code{GeometryGenerator.jar} to visualize the content of \code{roadDescription.xml}
\item The corresponding output will be written to folder ``\textbf{openDRIVEData}''
\begin{itemize}
\item OpenDRIVE file
\item Point list
\end{itemize}
\item A road description contains a sequence of geometries:
\begin{itemize}
\item \textsc{Line} is defined by \textit{length}
\item \textsc{Arc} is defined  by \textit{length} and \textit{curvature} (negative value $\rightarrow$ curvature to the right)
\item \textsc{Spiral} is defined by \textit{length}, \textit{start curvature} and \textit{end curvature} (=1/curvature)
\end{itemize}
\item The first geometry begins in (0,0,0) and is initially oriented to the east
\item Successor geometries start at the end of the previous geometry with the orientation in that point.
\item Number of lanes: templates available for the following road designs:
\begin{itemize}
\item ``1'' : one-lane road
\item ``2'': two-lane road
\item ``4'': four-lane road
\end{itemize}
\item Width: total width of the road (e.g. width of the lane in four-lane is width/4)
\item Speed limit: will be sent to \cd{} for control of the ego-vehicle
\end{itemize}

\begin{figure}[!ht]
\centering
\includegraphics[width=15cm]{fig/roadGen.png}
\caption{OpenDRIVE road generator.}\label{fig:roadGen}
\end{figure}

\subsection{Adding traffic}
To add a car to the scene write the following code to the \code{<traffic>..</traffic>} element of the corresponding \code{scenario.xml} file as in Fig.\ref{fig:trafficOpenDS}. Vehicle model can be selected among the predefined ones in the ``assets/Models/Cars/drivingCars/'' folder.

\begin{figure}[!ht]
\centering
\includegraphics[width=6cm]{fig/trafficOpenDS.png}
\caption{Traffic in OpenDS.}\label{fig:trafficOpenDS}
\end{figure}
%
Direction is set automatically according to lane orientation.


\subsection{Advanced settings}
The \textbf{startProperties.properties} file can be used to:
\begin{itemize}
\item Skip the resolution setup screen (\code{showsettingsscreen=true})
\item Set the default resolution (width, height)
\item Set the default driving task (e.g. \code{drivingtask=assets/DrivingTasks/Projects/track4/track4.xml})
\end{itemize}
%
The default driving task can furthermore be provided as command line arguments, e.g:\\
\code{java -jar OpenDS.jar assets/DrivingTasks/Projects/track4/track4.xml}

% \section{Compiling OpenDS}

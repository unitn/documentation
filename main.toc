\contentsline {chapter}{\numberline {1}Acronyms}{1}{chapter.1}%
\contentsline {chapter}{\numberline {2}Interfaces versions}{2}{chapter.2}%
\contentsline {chapter}{\numberline {3}\texttt {Co-driver}{} work-flow}{3}{chapter.3}%
\contentsline {section}{\numberline {3.1}Running \texttt {Co-driver}{}}{3}{section.3.1}%
\contentsline {subsection}{\numberline {3.1.1}The root path}{4}{subsection.3.1.1}%
\contentsline {subsection}{\numberline {3.1.2}Using the server}{4}{subsection.3.1.2}%
\contentsline {subsection}{\numberline {3.1.3}Using the S-Function}{6}{subsection.3.1.3}%
\contentsline {subsection}{\numberline {3.1.4}Using the library}{7}{subsection.3.1.4}%
\contentsline {subsection}{\numberline {3.1.5}Configuration Functions}{8}{subsection.3.1.5}%
\contentsline {subsection}{\numberline {3.1.6}Client Functions}{8}{subsection.3.1.6}%
\contentsline {subsection}{\numberline {3.1.7}Server Functions}{9}{subsection.3.1.7}%
\contentsline {subsection}{\numberline {3.1.8}Setting the parameters of conf.json}{9}{subsection.3.1.8}%
\contentsline {subsection}{\numberline {3.1.9}Converting the binary logged files}{9}{subsection.3.1.9}%
\contentsline {subsection}{\numberline {3.1.10}Re-generating log file with updated \texttt {Co-driver}{} releases}{9}{subsection.3.1.10}%
\contentsline {chapter}{\numberline {4}\texttt {Co-driver}{} Dynamics}{10}{chapter.4}%
\contentsline {section}{\numberline {4.1}Vehicle control}{10}{section.4.1}%
\contentsline {subsection}{\numberline {4.1.1}Longitudinal dynamics}{10}{subsection.4.1.1}%
\contentsline {subsection}{\numberline {4.1.2}Lateral dynamics}{11}{subsection.4.1.2}%
\contentsline {chapter}{\numberline {5}Public binaries}{12}{chapter.5}%
\contentsline {section}{\numberline {5.1}Main structure of the repository}{12}{section.5.1}%
\contentsline {section}{\numberline {5.2}Up to and including version 8.8.1204}{12}{section.5.2}%
\contentsline {subsection}{\numberline {5.2.1}Files organization}{13}{subsection.5.2.1}%
\contentsline {subsubsection}{client\_test\_example folder}{14}{section*.2}%
\contentsline {subsection}{\numberline {5.2.2}Up to and including version 9.0}{15}{subsection.5.2.2}%
\contentsline {subsection}{\numberline {5.2.3}From version 9.4 onwards}{16}{subsection.5.2.3}%
\contentsline {subsection}{\numberline {5.2.4}Files organization}{16}{subsection.5.2.4}%
\contentsline {chapter}{\numberline {6}\texttt {Co-driver}{} configuration}{17}{chapter.6}%
\contentsline {section}{\numberline {6.1}Communication parameters}{17}{section.6.1}%
\contentsline {section}{\numberline {6.2}Internal Parameters}{18}{section.6.2}%
\contentsline {subsection}{\numberline {6.2.1}Internal Configuration Parameters}{18}{subsection.6.2.1}%
\contentsline {subsection}{\numberline {6.2.2}General Parameters}{18}{subsection.6.2.2}%
\contentsline {subsection}{\numberline {6.2.3}Optimal Control Parameters}{19}{subsection.6.2.3}%
\contentsline {subsection}{\numberline {6.2.4}Velocity Thresholds}{19}{subsection.6.2.4}%
\contentsline {subsection}{\numberline {6.2.5}Obstacles Parameters}{19}{subsection.6.2.5}%
\contentsline {subsection}{\numberline {6.2.6}Lookahead Distance Parameters}{20}{subsection.6.2.6}%
\contentsline {subsection}{\numberline {6.2.7}Lateral Salience Parameters}{20}{subsection.6.2.7}%
\contentsline {subsection}{\numberline {6.2.8}Codriver stimuli bias}{20}{subsection.6.2.8}%
\contentsline {subsection}{\numberline {6.2.9}Neural Network for Pedestrian Safety Parameters}{21}{subsection.6.2.9}%
\contentsline {section}{\numberline {6.3}Action Selection}{21}{section.6.3}%
\contentsline {section}{\numberline {6.4}Low level models}{22}{section.6.4}%
\contentsline {subsection}{\numberline {6.4.1}Combined low level control}{22}{subsection.6.4.1}%
\contentsline {subsection}{\numberline {6.4.2}Longitudinal low level control}{23}{subsection.6.4.2}%
\contentsline {subsubsection}{Parameters of the low level control}{23}{section*.3}%
\contentsline {subsection}{\numberline {6.4.3}Lateral low level control}{24}{subsection.6.4.3}%
\contentsline {subsubsection}{Parameters of the low level control}{25}{section*.4}%
\contentsline {section}{\numberline {6.5}Filter}{25}{section.6.5}%
\contentsline {chapter}{\numberline {7}\texttt {Co-driver}{} Scaling}{26}{chapter.7}%
\contentsline {section}{\numberline {7.1}Example of scaling}{26}{section.7.1}%
\contentsline {subsection}{\numberline {7.1.1}Scale using the scenario}{26}{subsection.7.1.1}%
\contentsline {section}{\numberline {7.2}Scale the maneuver message}{28}{section.7.2}%
\contentsline {chapter}{\numberline {8}Running the \texttt {Co-driver}{} in OpenDS}{29}{chapter.8}%
\contentsline {section}{\numberline {8.1}Getting the OpenDS}{29}{section.8.1}%
\contentsline {subsection}{\numberline {8.1.1}Getting started}{29}{subsection.8.1.1}%
\contentsline {subsection}{\numberline {8.1.2}Manually running the vehicle}{30}{subsection.8.1.2}%
\contentsline {subsection}{\numberline {8.1.3}Running \texttt {Co-driver}{} in OpenDS}{30}{subsection.8.1.3}%
\contentsline {section}{\numberline {8.2}Creating the scenario}{30}{section.8.2}%
\contentsline {subsection}{\numberline {8.2.1}Assets folder}{30}{subsection.8.2.1}%
\contentsline {subsection}{\numberline {8.2.2}Task Description Files}{31}{subsection.8.2.2}%
\contentsline {subsection}{\numberline {8.2.3}OpenDRIVE Format}{32}{subsection.8.2.3}%
\contentsline {subsection}{\numberline {8.2.4}Advanced Road Generation}{33}{subsection.8.2.4}%
\contentsline {subsection}{\numberline {8.2.5}Basic Road Generation}{34}{subsection.8.2.5}%
\contentsline {subsection}{\numberline {8.2.6}Adding traffic}{35}{subsection.8.2.6}%
\contentsline {subsection}{\numberline {8.2.7}Advanced settings}{36}{subsection.8.2.7}%
